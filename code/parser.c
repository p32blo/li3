#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <ctype.h>

#include "parser.h"

/* tirar whitspace a esquerda */
char *ltrim(char *s)
{
	while (s && *s && isspace(*s)) {
		s++;
	}
	return s;
}

/* tirar whitspace a direita */
char *rtrim(char *s)
{
	int len;
	char *res;

	if (s && *s){
		len = strlen(s);
		res = s + len - 1;

		while(len > 0 && isspace(*res)){
			*res = '\0'; /* WARN - Nao usar com 'const char*' */
			res--; len--;
		}
	}

	return s;
}

/* tirar whitspace */
char *trim(char *s)
{
	return (rtrim(ltrim(s)));
}

char *get_extension(char *str)
{
	char *aux;
	int len;

	if (!str)
		return NULL;

	len = strlen(str);
	aux = str;

	for (aux += len - 1; *aux != '.' && len >= 0; aux--){
		len--;
	}

	return (len > 0)? aux : NULL;
}

int check_extension(char *str, char *ext)
{
	char *aux;

	if (!str)
		return 0;

	aux = get_extension(str);

	/* aux != NULL */
	return (aux && strcasecmp(aux, ext) == 0);
}

/* wrapper do atoi, com deteccao de erros na string recebida*/
int is_number(char *str)
{
	int i = 0;

	if (str && *str != '\0'){

		for(i = 0 ; str[i] != '\0' ; i++){
			if (!isdigit(str[i])){
				return -1;
			}
		}

		/* WARN - numeros negativos */
		i = atoi(str);

		return i;
	}

	return -1;
}

/* wrapper do atoi, com deteccao de erros na string recebida*/
int is_not_number(char *str)
{
	int i = 0;

	if (str && *str != '\0'){

		for(i = 0 ; str[i] != '\0' ; i++){
			if (isdigit(str[i])){
				return -1;
			}
		}

		/* WARN - numeros negativos */
		i = atoi(str);

		return i;
	}

	return -1;
}

int str_find(char *str, const char *word)
{
	int i = 0, j = 0;

	while(word[j] != '\0') {

		if(str[i] == '\0')
			return 0;

		if (word[j] == tolower(str[i])){
			j++;
		} else {
			j = 0;
		}

		i++;
	}

	return 1;
}

/* Ver se qualquer uma destas palavras esta contida em str*/
static int ignore_title (char *str)
{
	int res = 0;

	if (!str)
		return res;


	if (str_find(str, "editorial")){
		res++;
	}
	if (!res && str_find(str, "preface")){
		res++;
	}
	if (!res && str_find(str, "errata")){
		res++;
	}
	if (!res && str_find(str, "obituary")){
		res++;
	}
	if (!res && str_find(str, "in memory of")){
		res++;
	}
	if (!res && str_find(str, "isbn")){
		res++;
	}
#if DEBUG
	if (res)
		printf("<<<");
#endif

	return res;
}

/* funcao que procura o PRIMEIRO 'delim' em '*str'*/
char *tokanizer(char **str, char delim)
{
	char *pt = NULL;
	char *res = NULL;

	pt = *str;

	if (!pt)
		return NULL;

	pt = strchr(pt, delim);

	if (pt){
		res = *str;

		*pt = '\0';
		*str = ++pt;
	}

	return res;
}

/* wrapper the funcao anterior que aplica o trim as strings */
char * next_tok (char **ptr, char delim)
{
	char *res = tokanizer(ptr, delim);

	res = trim(res);
	*ptr = trim(*ptr);

#if DEBUG
	printf("'%s' ", res);
#endif

	return res;
}

/* wrapper the funcao anterior que devolve um numero lido */
int next_tok_int(char **ptr, char delim)
{
	char *res;
	int num = -1;

	res = tokanizer(ptr, delim);

	res = trim(res);
	*ptr = trim(*ptr);

#if DEBUG
	printf("'%s' ", res);
#endif

	if (res) {
		num = is_number(res);
	}

	return num;
}

/* funcao que procura o ULTIMO 'delim' em '*str'*/
char *tokanizer_r(char **str, char delim)
{
	char *pt = NULL;
	char *res = NULL;

	pt = *str;

	if (!pt)
		return NULL;

	pt = strrchr(pt, delim);

	if (pt){
		res = *str;

		*pt = '\0';
		*str = ++pt;
	}

	return res;
}

/* wrapper the funcao anterior que aplica o trim as strings */
char * next_tok_r (char **ptr, char delim)
{
	char *res = tokanizer_r(ptr, delim);

	res = trim(res);
	*ptr = trim(*ptr);

#if DEBUG
	printf("'%s' ", res);
#endif

	return res;
}

/* wrapper the funcao anterior que devolve um numero lido */
int next_tok_int_r(char **ptr, char delim)
{
	char *res;
	int num = -1;

	res = tokanizer_r(ptr, delim);

	res = trim(res);
	*ptr = trim(*ptr);

#if DEBUG
	printf("'%s' ", res);
#endif

	if (res) {
		num = is_number(res);
	}

	return num;
}

/* ver se todos os autores existem em todos o scampos */
static int check_autores(char *str)
{
	char *aux;
	int res = 1;

	while((aux = next_tok(&str, ','))){
		if (*aux == '\0') {
			res = 0;
		} else {
			res++;
		}
	}

	if (!str || *str == '\0')
		res = 0;

	return res;
}

/* Parsing dum Artigo do tipo JOURNAL */
Line parce_journal(char *str)
{
	char *buf, *tok;
	int start, end;
	int err, nauth, id, ano;
	Line res = {-1, -1, -1, -1};

	ano = nauth = id = start = end = err = 0;

	buf = trim(str);

	if ((id = next_tok_int(&buf, ' ')) == -1) err = 1; /* numero */

	if (!err && (!(tok = next_tok(&buf, ':')) || *tok == '\0' || !(nauth = check_autores(tok)))) err = 1; /* autores */
	if (!err && (!(tok = next_tok(&buf, '.')) || *tok == '\0' || ignore_title(tok))) err = 1; /* titulo */

	if (!err && (!(tok = next_tok(&buf, '(')) || *tok == '\0')) err = 1; /* nome da revista */
	if (!err && (!(tok = next_tok(&buf, ')')) || *tok == '\0')) err = 1;	/* sigla */

	if (!err && (next_tok_int(&buf, '(') == -1)) err = 1; /* volume revista */
	if (!err && (next_tok_int(&buf, ')') == -1)) err = 1; /* numero da revista */

	if (!err && (!(tok = next_tok(&buf, ':')) || *tok != '\0')) err = 1; /* verificar se existem o caracter ':' */

	if (!err && ((start = next_tok_int(&buf, '-')) == -1)) err = 1; /* pagina inicial */
	if (!err && ((end = next_tok_int(&buf, '(')) == -1)) err = 1;  /* pagina final */
	if (!err && (ano = next_tok_int(&buf, ')')) == -1) err = 1; /* ano */

	/* Se houver buf para alem deste ponto dar erro */
	if (buf && *buf != '\0') err = 1;

	if(!err){
		res.id = id;
		res.year = ano;
		res.n_authors = nauth;
		res.n_pages = end - start + 1;
	}

	return res;
}


/* Parsing dum Artigo do tipo CONV */
Line parce_conf(char *str)
{
	char *buf, *tok, *subtok;
	int start, end;
	int err, id, nauth, ano;
	Line res = {-1, -1, -1, -1};

	ano = id = nauth = start = end = err = 0;

	buf = trim(str);

	if ((id = next_tok_int(&buf, ' ')) == -1) err = 1; /* numero */

	if (!err && (!(tok = next_tok(&buf, ':')) || *tok == '\0'|| !(nauth = check_autores(tok)))) err = 1; /* autores */
	if (!err && (!(tok = next_tok(&buf, '.')) || *tok == '\0' || ignore_title(tok))) err = 1; /* titulo */

	if (!err && (!(tok = next_tok_r(&buf, ':')))) err = 1; /* para ignorar os espacos no nome da conferencia */
	if (!err && (!tok || !(subtok = next_tok_r(&tok, ' ')) || is_not_number(subtok))) err = 1; /* nome da conferencia */
	if (!err && (!tok || (ano = is_number(trim(tok))) == -1)) err = 1; /* ano */

	if (!err && ((start = next_tok_int(&buf, '-')) == -1)) err = 1; /* pagina inicial */
	if (!err && ((end = is_number(trim(buf))) == -1)) err = 1;  /* pagina final - o que sobra do buf */

	if(!err){
		res.id = id;
		res.year = ano;
		res.n_authors = nauth;
		res.n_pages = end - start + 1;
	}

	return res;
}
