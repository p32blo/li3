#ifndef __QUERY_H__
#define __QUERY_H__

#include <stdio.h>

#include "hash.h"
#include "parser.h"

typedef struct sTotals{
	int *total;
	int min, max;
	HashTable *htable;
}Totals;

Totals *totals_init(int table_size, int totals_size);
void free_totals(Totals *tot);
void process_line(Totals *tot, Line *l);
void print_tot(Totals *tot);

void write_csv(Totals *tot, char *filename);

#endif /* __QUERY_H__ */
