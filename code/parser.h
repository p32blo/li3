#ifndef __PARSER_H__
#define __PARSER_H__

typedef struct sLine{
	int id, year;
	int n_authors;
	int n_pages;
}Line;

char *ltrim(char *s);
char *rtrim(char *s);
char *trim(char *s);

int is_number(char *str);

char *get_extension(char *str);
int check_extension(char *str, char *ext);

char *tokanizer(char **str, char delim);
char *tokanizer_r(char **str, char delim);

char *next_tok (char **ptr, char delim);
char *next_tok_r (char **ptr, char delim);

int next_tok_int(char **ptr, char delim);
int next_tok_int_r(char **ptr, char delim);

Line parce_journal(char *str);
Line parce_conf(char *str);

#endif /* __PARSER_H__ */
