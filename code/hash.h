
typedef int KeyType;
typedef int *Info;

typedef struct sEntry {
	KeyType key;
	Info info;
	struct sEntry *next;
} Entry;

typedef struct sHashTable {
	int size, nelems;
	Entry **table;
} HashTable;

HashTable *initializeTable(int size);
void freeHTable (HashTable *h);

Entry *insertTable(HashTable *h, KeyType k, Info info);
Entry *retrieveTable(HashTable *h, KeyType k);

void print_hash(HashTable *h);

