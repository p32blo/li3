
#include <stdlib.h>

#include "query.h"
#include "parser.h"

Totals *totals_init(int table_size, int totals_size)
{
	Totals *tot = malloc(sizeof(Totals));
	int i;

	if (tot){
		tot->min = 0;
		tot->max = 0;

		tot->htable = initializeTable(table_size);
		tot->total = malloc(sizeof(int) * totals_size);

		if (tot->total){
			tot->total[0] = totals_size;

			for (i = 1; i < totals_size; ++i) {
				tot->total[i] = 0;
			}
		}
	}
	return tot;
}


void free_totals(Totals *tot)
{
	Entry *aux = NULL;
	int i;

	for (i = 0; i < tot->htable->size; ++i) {
		if (tot->htable->table[i]){
			for (aux = tot->htable->table[i]; aux; aux = aux->next) {
				free(aux->info);
			}
		}
	}

	free(tot->total);
	freeHTable(tot->htable);
	free(tot);
}

static void inc_tot(int **arr, int pos)
{
	int *node = NULL;
	int *aux = *arr;
	int prev, size;
	int i;

	if (!aux){
		prev = 0;
		size = 4;
	} else {
		prev = size = aux[0];
	}


	if (!aux || pos >= size){

		while(pos >= size){
			size *= 2;
		}

		node = realloc(aux, sizeof(int) * size);
		if (node){
			aux = node;
			for (i = prev; i < size; ++i) {
				aux[i] = 0;
			}
			aux[0] = size;
		} else {
			free(aux);
		}
	}
	aux[pos] += 1;

	*arr = aux;
}

void process_line(Totals *tot, Line *l)
{
	int * aux = NULL;
	Entry *entry = insertTable(tot->htable, l->year, aux);
	inc_tot(&(tot->total), l->n_authors);
	inc_tot(&(entry->info), l->n_authors);

	if(tot->htable->nelems == 1){
		tot->min = tot->max = l->year;
	} else{
		if (l->year < tot->min) {
			tot->min = l->year;
		}
		if (l->year > tot->max){
			tot->max = l->year;
		}
	}
}

void write_interval(FILE *fp, Totals *tot, int begin, int end)
{
	Entry *entry = NULL;
	int i, j;

	if (!fp)
		return;

	fprintf(fp,"\"ano\",\"#autores\",\"#artigos\"\n");
	for (i = begin; i <= end; ++i) {
		entry = retrieveTable(tot->htable, i);
		if(entry){
			for (j = 1; j < entry->info[0]; ++j) {
				if (entry->info[j]){
					fprintf(fp, "\"%d\",\"%d\",\"%d\"\n", i, j, entry->info[j]);
				}
			}
		}
	}

}

void write_totals(FILE *fp, Totals *tot)
{
	int i;

	if (!fp)
		return;

	fprintf(fp, "\"#autores\",\"#artigos\"\n");
	for (i = 1; i < tot->total[0]; ++i) {
		if(tot->total[i]){
			fprintf(fp, "\"%d\",\"%d\"\n", i, tot->total[i]);
		}
	}

}

void write_query_interval(FILE *fp, Totals *tot, char *filename)
{
	Entry *entry = NULL;
	int i, j, res;
	FILE *f;
	int begin = -1, end = -1;

	char *line = NULL;
	char *aux = NULL;
	size_t n = 0;

	fprintf(fp, "\"intervalo\",\"#artigos\"\n");

	f = fopen(filename, "r");

	if (!f) {
		return;
	}

	while (getline(&line, &n, f) > 0){
		aux = trim(line);
		begin = next_tok_int(&aux, '-');
		if (begin != -1){
			end = is_number(aux);
		}
		if (end != -1){
			res = 0;
			for (i = begin; i <= end; ++i) {
				entry = retrieveTable(tot->htable, i);
				if(entry){
					for (j = 1; j < entry->info[0]; ++j) {
						if (entry->info[j]){
							res += entry->info[j];
						}
					}
				}
			}
			fprintf(fp, "\"%d-%d\",\"%d\"\n", begin, end, res);
		}
	}
	fclose(f);
	free(line);
}

void write_query_percentage(FILE *fp, Totals *tot, char *filename)
{
	Entry *entry = NULL;
	FILE *f;

	int i, total;
	int year = -1;

	char *line = NULL;
	char *aux = NULL;
	size_t n = 0;

	fprintf(fp, "\"ano\",\"#autores\",\"percentagem\"\n");

	f = fopen(filename, "r");

	if (!f) {
		return;
	}

	while (getline(&line, &n, f) > 0){
		aux = line;

		year = is_number(aux);
		if (year != -1){
			entry = retrieveTable(tot->htable, year);
			if(entry){
				total = 0;
				for (i = 1; i < entry->info[0]; ++i) {
					total += entry->info[i];
				}

				for (i = 1; i < entry->info[0]; ++i) {
					if (entry->info[i]){
						fprintf(fp, "\"%d\",\"%d\",\"%.2f\"\n", year, i, (float) entry->info[i] / total * 100);
					}
				}
			}
		}
	}
	fclose(f);
	free(line);
}

void write_csv(Totals *tot, char *filename)
{

	FILE *fp;

	fp = fopen(filename, "w");

	if (!fp)
		return;

	write_interval(fp, tot, tot->min, tot->max);
	write_totals(fp, tot);
	write_query_interval(fp, tot, "datas3.txt");
	write_query_percentage(fp, tot, "datas4.txt");

	fclose(fp);
}

void print_tot(Totals *tot)
{
	int i;
	printf("totals(size: %d, min = %d, max = %d):\n", tot->total[0], tot->min, tot->max);
	for (i = 1; i < tot->total[0]; ++i) {
		if(tot->total[i]){
			printf("%d -> %d\n", i, tot->total[i]);
		}
	}
}
