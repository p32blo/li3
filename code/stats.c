#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include "stats.h"
#include "parser.h"

/********************************************/
/* Funcoes para o manuseamento da estrutura */
/********************************************/

#if DEBUG
static char *str_article(Article a)
{
	char *res = NULL;

	switch (a) {
	case CONF:
		res = "CONF";
		break;
	case JOURNAL:
		res = "JOURNAL";
		break;
	case ERROR:
		res = "ERROR";
		break;
	}
	return res;
}

/* Imprimir todos os ficheiros lidos na estrutura 'Files'*/
void print_files(Stats *s)
{
	int i;
	for (i = 0; i < s->nelem; i++) {
		printf("|%s|%d|%d|%s| -> ", str_article(s->files[i].type),
				s->files[i].artigos, s->files[i].rejeitados,
				s->files[i].filename);
	}

	printf("\n");
}
#endif

/* Inicializar a estrutura de dados */
Stats* stat_init ()
{
	Stats *stats = malloc(sizeof(Stats));

	if (stats){
		stats->files = NULL;
		stats->min_lines = 0;
		stats->nelem = 0;

		stats->files = malloc(sizeof(Files) * 2);
		if (stats->files)
			stats->size = 2;
		else
			stats->size = 0;
	}

	return stats;
}


/* Libertar a memoria de estrutura 'Files' */
static void free_files (Stats *st)
{
	int i = 0;
	for (i = 0; i < st->nelem; i++) {
		free(st->files[i].filename);
	}
	free(st->files);
}

/* Libertar a memoria da estrutura principal */
void free_stats(Stats *st)
{
	free_files(st);
	free(st);
}

static Article get_article_type(char *str)
{
	int i;
	Article res;

	for (i = 0; i < 2 && str[i] != '\0'; i++);

	if (str[i] == '\0')
		res = ERROR;
	else if (str[1] != '-')
		res =  ERROR;
	else{
		switch (str[0]) {
			case 'c':
				res = CONF;
				break;
			case 'j':
				res = JOURNAL;
				break;
			default:
				res = ERROR;
				break;
		}
	}

	return res;
}

static Article get_article(char *str)
{
	if (!check_extension(str, ".txt"))
		return ERROR;

	return get_article_type(str);
}

/***************************/
/* Geracao de estatisticas */
/***************************/


/* Inserir um novo ficeiro na estrutura de dados */
void insert_file(Stats **s, char *filename)
{
	Files *node = NULL;
	Stats *aux = NULL;

	if (!(*s))
		return;

	aux = *s;

	if (aux->nelem >= aux->size) {
		node = realloc(aux->files, sizeof(Files) * 2 * aux->size);

		if (node) {
			aux->files = node;
			aux->size *= 2;
		}else{
			free(aux->files);
			return;
		}

	}

	aux->files[aux->nelem].type = get_article(filename);
	aux->files[aux->nelem].filename = strdup(filename);
	aux->files[aux->nelem].artigos = 0;
	aux->files[aux->nelem].rejeitados = 0;

	aux->nelem++;

	*s = aux;
}

/* XXX - Será que é necessário passar por string 1º ? */
/* XXX - asprintf? */
/* D.txt - string com as estatisticas basicas tiradas das estruturas */
char* str_stats(Stats *s)
{
	int i,  entradas , rejeitados, artigos, revistas, conferencias;
	char *str = NULL;
	char *format =	"Estatistica basica\n"
					"------------------\n"
					"%d entradas\n"
					"%d rejeitadas\n"
					"%d artigos\n"
					"  %d em revista\n"
					"  %d em conferencia\n";

	entradas = rejeitados = artigos = revistas = conferencias = 0;

	for (i = 0; i < s->nelem; i++) {
		rejeitados += s->files[i].rejeitados;

		if (s->files[i].type == CONF){
			conferencias += s->files[i].artigos;
		}
		else if(s->files[i].type == JOURNAL){
			revistas += s->files[i].artigos;
		}

	}

	artigos = revistas + conferencias;
	entradas = rejeitados + artigos;

	i = asprintf(&str, format, entradas, rejeitados, artigos, revistas, conferencias);
	if (i != -1)
		return str;
	else
		return NULL;
}

/* E.txt - string com as listas rejeitadas tiradas das estruturas */
char* str_stats_rej(Stats *s)
{
	int i, p = 0;
	char *line = NULL;
	char *str =  strdup("Lista Rejeitadas\n"
						"----------------\n");

	for (i = 0; i < s->nelem && p != -1 ; i++) {
		if (s->files[i].type != ERROR)
			p = asprintf(&line, "%s%s %d\n", str, s->files[i].filename, s->files[i].rejeitados);
		else
			p = asprintf(&line, "%s%s %s\n", str, s->files[i].filename, "X");
		if (p) {
			free(str);
			str = line;
		}
	}

	if (p != -1)
		return str;
	else
		return NULL;
}

/* escrever as estatisticas em disco */
void write_stats(Stats *s, char *filename)
{
	FILE *fp;

	char *str;

	fp = fopen(filename, "w");
	str = str_stats(s);

	if (fp && str) {
		fwrite(str, sizeof(char), strlen(str), fp);
	}
	fclose(fp);
	free(str);
}

/* escrever as estatisticas em disco */
void write_stats_rej(Stats *s, char *filename)
{
	FILE *fp;

	char *str;

	fp = fopen(filename, "w");
	str = str_stats_rej(s);

	if (fp && str) {
		fwrite(str, sizeof(char), strlen(str), fp);
	}
	fclose(fp);
	free(str);
}
