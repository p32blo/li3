
#include <stdio.h>
#include <stdlib.h>

#include "hash.h"

/* funcao de hash */
static int hash(HashTable *h, KeyType k)
{
	int res = k;
	return res % h->size;
}

/* inicializa a tabela de hash */
HashTable *initializeTable(int size)
{
	int i;
	HashTable *h = malloc(sizeof(HashTable));

	if(h){
		h->nelems = 0;
		h->size = size;
		h->table = malloc (sizeof(Entry*) * h->size);

		for (i = 0; i < h->size; ++i) {
			h->table[i] = NULL;
		}
	}

	return h;
}

static void clearEntry(Entry *e){
	if (e){
		clearEntry(e->next);
		free(e);
	}
}

/* limpa a tabela de hash */
void clearTable(HashTable *h)
{
	int i;
	for (i = 0; i < h->size; ++i) {
		clearEntry(h->table[i]);
		h->table[i] = NULL;

	}
	h->nelems = 0;
}

void freeHTable (HashTable *h)
{
	clearTable(h);
	free(h->table);
	free(h);
}

/* procura na tabela o elemento de chave k, e retorna o apontador
   para a celula aonde a chave se encontra (ou NULL caso k nao exista)*/
Entry *retrieveTable(HashTable *h, KeyType k)
{
	int i = hash(h, k);
	Entry *res;
	for (res = h->table[i]; res; res = res->next){
		if (res->key == k)
			return res;
	}
	return NULL;
}



/* insere uma nova associacao entre uma chave nova e a restante informacao */
static Entry *insert(HashTable *h, KeyType k, Info info)
{
	int i = hash(h, k);

	Entry *aux = retrieveTable(h, k);

	if (aux == NULL){

		Entry *entry = malloc(sizeof(Entry));

		entry->key = k;
		entry->info = info;

		entry->next = h->table[i];
		h->table[i] = entry;
		h->nelems++;

		return entry;

	} else {
		return aux;
	}
}

static void re_insert(HashTable *h, Entry *entry)

{
	int i = hash(h, entry->key);

	entry->next = h->table[i];
	h->table[i] = entry;
	h->nelems++;
}

static void re_insert_entry(HashTable *h, HashTable *h_new, Entry *entry)
{
	if (entry){
		re_insert_entry(h, h_new, entry->next);
		re_insert(h_new, entry);
	}
}

static void reHash(HashTable *h, int size)
{
	HashTable *h_new = initializeTable(size);
	int i;

	for (i = 0; i < h->size; ++i) {
		re_insert_entry(h, h_new, h->table[i]);
	}

	/*clearTable(h);*/
	free(h->table);

	h->nelems = h_new->nelems;
	h->size = size;
	h->table = h_new->table;

	free(h_new);
}


static void check_reHash(HashTable *h)
{
	float factor = (float) h->nelems / (float) h->size;

	if (factor >= 0.75){
		reHash(h, h->size * 2);
	}
	/* Nunca se apaga por isso nao precisa de decrescer */
	/*
	else if (factor < 0.25){
		reHash(h, h->size / 2);
	}
	*/
}


Entry *insertTable(HashTable *h, KeyType k, Info info)
{
	Entry* res = insert(h, k, info);
	check_reHash(h);

	return res;
}


/* apaga o elemento de chave k da tabela */
void deleteTable(HashTable *h, KeyType k)
{
	int i = hash(h, k);
	Entry *tail;
	Entry *aux;
	Entry *head = retrieveTable(h, k);
	
	if (!head){
		return;
	}
	
	if (h->table[i]->key == k){
		aux = h->table[i];
		h->table[i] = h->table[i]->next;
		h->nelems--;
		free(aux);
		return;
	}
	
	head = h->table[i];
	for (tail = h->table[i]->next; tail; tail = tail->next) {
		if (tail->key == k){
			aux = head->next;
			
			head->next = tail->next;
			
			free(aux);
			break;
		}
		head = tail;
	}
	h->nelems--;
	check_reHash(h);
}	

void print_hash(HashTable *h)
{
	int i, j;
	Entry *aux;
	float factor = (float)h->nelems / (float) h->size;
	printf("nelems: %3d\n  size: %3d\n factor: %3f\n", h->nelems, h->size, factor);

	for (i = 0; i < h->size; ++i) {
		printf("[%3d] ", i);
		
		for (aux = h->table[i]; aux; aux = aux->next) {
			printf("-> %d : [", aux->key);
			for (j = 1; j < aux->info[0]; ++j) {
				printf("%d, ", aux->info[j]);
			}
			printf("]");
		}
		printf("\n");
	}
	printf("\n");
}

/*
int main()
{
	HashTable *h = malloc(sizeof(HashTable));
	
	int *i = malloc(sizeof(int));
	*i = 4;
	initializeTable(h, 4);
	
	*i = 32;
	insertTable(h, 0, i); *i = 25;
	insertTable(h, 1, i); *i = 110;
	insertTable(h, 2, i); *i = 0;
	print_hash(h);

	deleteTable(h, 2);
	deleteTable(h, 0);
	print_hash(h);
	
	insertTable(h, 0, i);*i = 1;
	insertTable(h, 1, i);*i = 2;
	insertTable(h, 2, i);*i = 3;
	insertTable(h, 3, i);*i = 4;
	insertTable(h, 4, i);*i = 5;
	insertTable(h, 5, i); *i = 6;
	insertTable(h, 6, i);*i = 7;
	insertTable(h, 7, i);*i = 8;
	insertTable(h, 8, i);
	print_hash(h);
	
	reHash(h, 10);
	print_hash(h);

	reHash(h, 1);
	print_hash(h);

	clearTable(h);
	print_hash(h);

	free(i);
	free(h->table);
	free(h);

	return 0;
}
*/
