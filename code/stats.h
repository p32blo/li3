#ifndef __STATS_H__
#define __STATS_H__

#include "query.h"

typedef enum eArticle {
	ERROR = -1,
	CONF,
	JOURNAL
}Article;

typedef struct sFiles {

	Article type;
	char *filename;

	int artigos, rejeitados;

} Files;

typedef struct sStats {

	int min_lines;

	int nelem, size;
	Files *files;

}Stats;

/* Protipos */
Stats* stat_init ();
void free_stats(Stats *st);

char* str_stats(Stats *s);
char* str_stats_rej(Stats *s);

void write_stats(Stats *s, char *filename);
void write_stats_rej(Stats *s, char *filename);

int stats_read(Stats *stats, Totals *tot, char *filename);
int stats_gen(char *);

void insert_file(Stats **s, char *filename);
int val_line(char *str, int min, Article type);
#if DEBUG
void print_files(Stats *s);
#endif

#endif /*__STATS_H__*/
