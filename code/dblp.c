#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include "stats.h"
#include "query.h"


int main(int argc, char **argv)
{

	int i, val = 0;
	char *root_dir;
	char *file = "lista.txt";

	/* se nao tiver este precisa do ponto para correr */
	stats_gen(file);

	root_dir = getcwd(NULL, 0);

	for (i = 1; i < argc; i++) {

		if(chdir(root_dir) == 0) {

			/* mudar de diretoria */
			val = chdir(argv[i]);

			if (val == 0){
#if DEBUG
				printf("->>%s:\n", argv[i]);
#endif
				stats_gen(file);
			}
#if DEBUG
			else printf("%s : diretoria inexistente", argv[i]);
#endif
		}
	}

	if(chdir (root_dir) == 0)
		val = 0;
	else
		val = -1;

	free(root_dir);

#if DEBUG
	printf("\n- Finished -\n");
#endif
	return val;
}
