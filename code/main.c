#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <ctype.h>

#include "parser.h"
#include "stats.h"
#include "query.h"

/* ver se uma linha é valida e adiciona a estrutura  */
static void parse_line (Files *aux, Totals *tot, int min, char *str)
{
	Line val = {-1, -1, -1, -1};

	if(aux->type == JOURNAL)
		val = parce_journal(str);
	else if (aux->type == CONF)
		val = parce_conf(str);

#if DEBUG
	printf(" |%d|\n", val.n_pages);
	printf("id:%d aut:%d ano:%d pag:%d\n", val.id, val.n_authors, val.year, val.n_pages);
#endif

	if (0 <= val.n_pages && val.n_pages >= min) {
		aux->artigos++;
		process_line(tot, &val);
	} else {
		aux->rejeitados++;
	}
}

/* prenche a estrutura consoante as estatisticas */
static void parse_stats(Stats *s, Totals *tot)
{
	FILE *fp = NULL;
	size_t n = 0;
	char *str = NULL;
	char *aux = NULL;

	int i;

	for (i = 0; i < s->nelem; i++){
		fp = fopen(s->files[i].filename, "r");
		if (fp){
#if DEBUG
			printf("==== %s ==== |0|1| \n", s->files[i].filename);
#endif
			while(getline(&str, &n, fp) > 0){
				aux = trim(str);
				if(*aux != '\0'){
					parse_line(&s->files[i], tot, s->min_lines, aux);
				}
			}
			fclose(fp);

		} else s->files[i].type = ERROR;
	}
	free(str);
}

/* lista.txt - Ler o ficheir principal e gerar a lista de ficheiros a processar */
static int read_dir(Stats *s, char *filename)
{
	FILE *fp = NULL;
	size_t n = 0;

	char *line = NULL;
	char *min = NULL;
	char *aux = NULL;

	fp = fopen(filename, "r");

	if (!fp){
		/*- Not a valid file - */
		return 0;
	}

	if (getline(&min, &n, fp) > 0){
		s->min_lines = is_number(trim(min));

#if DEBUG
		printf("min: %d\n", s->min_lines);
#endif
		if (s->min_lines == -1){
#if DEBUG
			printf("invalid format: o primeira linha tem que conter o numero min de paginas");
#endif
			return 0;
		}

		while (getline(&line, &n, fp) > 0) {
			aux = trim(line);
			if (*aux != '\0')
				insert_file(&s, aux);
#if DEBUG
			print_files(s);
#endif
		}
	}

	fclose(fp);
	free(min);
	free(line);

	return s->nelem;
}

int stats_read(Stats *stats, Totals *tot, char *filename)
{
	int val = 0;
	val = read_dir(stats, filename);
	if (val){
		parse_stats(stats, tot);
	}
	return val;
}

int stats_gen(char *filename)
{
	Stats *stats;
	Totals *tot;

	int val, res = 0;

	if (!filename)
		return -1;

	stats = stat_init();
	tot = totals_init(5, 1);

	val = stats_read(stats, tot, filename);

	if (val){
		write_stats(stats, "D.txt");
		write_stats_rej(stats, "E.txt");
		write_csv(tot, "G.csv");

		/*print_tot(tot);
		print_hash(tot->htable);*/
	}
	else{
		res = -1;
	}

	free_stats(stats);
	free_totals(tot);

	return res;
}
