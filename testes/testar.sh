SCRIPTDIR=$( cd "$( dirname "$0" )" && pwd )
for DIR in ${SCRIPTDIR}/* ; do
	if [ -d "${DIR}" ]; then

		echo ${DIR}:
		../code/PARSER_LI3 ${DIR} > /dev/null

		echo -n "  E "
		if diff ${DIR}/E.txt ${DIR}/_E.txt > /dev/null; then
			echo ok
		else
			echo nao
		fi

		echo -n "  D "
		if diff ${DIR}/D.txt ${DIR}/_D.txt > /dev/null; then
			echo ok
		else
			echo nao
		fi

		echo -n "  G "
		if diff ${DIR}/G.csv ${DIR}/G.txt > /dev/null; then
			echo ok
		else
			echo nao
		fi


		rm -f ${DIR}/E.txt ${DIR}/D.txt ${DIR}/G.csv
	fi
done
